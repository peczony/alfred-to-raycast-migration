# Alfred to Raycast migration

This isn’t a complete migration script, but rather a helper. It can do three things: convert Raycast config file to json and back, and convert Alfred snippets to Raycast format.

Usage:

`python alfred_converter.py rayconfig_to_json "path/to/Raycast 2022-05-03 16.39.37.rayconfig"` → `path/to/Raycast 2022-05-03 16.39.37.json`

And vice versa,

`python alfred_converter.py json_to_rayconfig "path/to/Raycast 2022-05-03 16.39.37.json"` → `path/to/Raycast 2022-05-03 16.39.37.rayconfig` (if file already exists, the script won’t overwrite it, but will create a copy alongside it)

To migrate snippets do:

`python alfred_converter.py snippet path/to/your.alfredpreferences/snippets` → `snippets_2022-05-03T16:39:38Z.json`

Then you copy some of all of the snippets, paste them to Raycast JSON, then convert it back to rayconfig and import.
