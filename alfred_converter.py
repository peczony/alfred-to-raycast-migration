#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import json
import datetime
import argparse
import zlib

snippet_stub = {
    "category": "text",
    "copyCount": 1,
    "isPinned": False,
}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("action", choices=["snippet", "rayconfig_to_json", "json_to_rayconfig"])
    parser.add_argument("target")
    args = parser.parse_args()

    ts = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ")

    if args.action == "snippet":
        result = []
        for (root, _, files) in sorted(os.walk(args.target)):
            for fn in sorted(files):
                if not (fn.endswith(".json") and not fn.startswith((".", "~", "_"))):
                    continue
                full_path = os.path.join(root, fn)
                with open(full_path, encoding="utf8") as f:
                    old_snippet = json.loads(f.read())
                new_snippet = snippet_stub.copy()
                for k in ("createdAt", "accessedAt", "modifiedAt"):
                    new_snippet[k] = ts
                new_snippet["name"] = old_snippet["alfredsnippet"]["name"]
                new_snippet["text"] = old_snippet["alfredsnippet"]["snippet"]
                if not old_snippet["alfredsnippet"].get("dontautoexpand"):
                    new_snippet["alias"] = old_snippet["alfredsnippet"]["keyword"]
                result.append(new_snippet)
        with open(f"snippets_{ts}.json", "w", encoding="utf8") as f:
            f.write(json.dumps(result, indent=4, sort_keys=True))

    if args.action == "rayconfig_to_json":
        bn, ext = os.path.splitext(args.target)
        assert ext == ".rayconfig"
        new_filename = bn + ".json"
        if os.path.exists(new_filename):
            new_filename = bn + f"_{ts}.json"
        with open(args.target, "rb") as f:
            raybytes = f.read()
        unzipped = zlib.decompress(raybytes, zlib.MAX_WBITS | 16)
        with open(new_filename, "wb") as f:
            f.write(unzipped)

    if args.action == "json_to_rayconfig":
        bn, ext = os.path.splitext(args.target)
        assert ext == ".json"
        new_filename = bn + ".rayconfig"
        if os.path.exists(new_filename):
            new_filename = bn + f"_{ts}.rayconfig"
        with open(args.target, "rb") as f:
            to_compress = f.read()
        compressor = zlib.compressobj(9, zlib.DEFLATED, zlib.MAX_WBITS | 16)
        compressed = compressor.compress(to_compress) + compressor.flush()
        with open(new_filename, "wb") as f:
            f.write(compressed)


if __name__ == "__main__":
    main()
